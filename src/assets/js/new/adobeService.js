(function(jQuery, app, window, document, undefined) {
    'use strict'

    var adobeService = app.adobeService = app.adobeService || (function() {
       
        var BASE_URI = 'https://api3.omniture.com/admin/1.4/rest/';

        var ADOBE_METRIC_TYPES = {
             TYPE_ELEMENT: 'element',
             TYPE_METRIC: 'metric',
             TYPE_SEGMENT: 'segment'
        }

        var username = '',
            password = '',
            reportSuiteId = '';
        
        var makeRequest = function (payload, endpoint, callback, metricType)
        {
            var headers = new Wsse().generateAuth(username, password);
            jQuery.ajax(endpoint, {
                type:'POST',
                data: payload,
                success: function(data){
                    if(metricType === ADOBE_METRIC_TYPES.TYPE_SEGMENT){
                        data = data[0]['segments'];
                    }
                    callback(data, metricType)
                },
                error: function(error){
                    console.log(error);
                },
                dataType: "json",
                headers: {
                    'X-WSSE': headers['X-WSSE']
                }
            });
        };

        var setReportSuiteId = function(adobeReportSuiteId){
            reportSuiteId = adobeReportSuiteId;
        };

        var getSegments = function(callback){
            var endpoint = BASE_URI + '?method=ReportSuite.GetSegments';
            var payload = { "rsid_list":[ reportSuiteId ] };
            makeRequest(payload, endpoint, callback, ADOBE_METRIC_TYPES.TYPE_SEGMENT);
        };

        var getMetrics = function(callback){
            var endpoint = BASE_URI + '?method=Report.GetMetrics';
            var payload = { "reportSuiteID": reportSuiteId };
            makeRequest(payload, endpoint, callback, ADOBE_METRIC_TYPES.TYPE_METRIC);
        };

        var getElements = function(callback){
            var endpoint = BASE_URI + '?method=Report.GetElements';
            var payload = { "reportSuiteID": reportSuiteId };
            makeRequest(payload, endpoint, callback, ADOBE_METRIC_TYPES.TYPE_ELEMENT);
        };

        var getAdobeTypes = function(){
            return ADOBE_METRIC_TYPES;
        }

        var saveCredentials = function(adobeUsername, adobePassword){
            username = adobeUsername;
            password = adobePassword;
        };

        var saveCredentialsToLocal = function(adobeUsername, adobePassword){
            saveCredentials(adobeUsername, adobePassword);

            var adobeCredentials = {
                username: adobeUsername,
                password: adobePassword
            };

            if (localStorage.getItem("adobeCredentials") === null) {
                localStorage.removeItem('adobeCredentials');
            }            
            
            localStorage.setItem('adobeCredentials', JSON.stringify(adobeCredentials));
        }

        var getCredentialsFromLocal = function(){
            var credentials = localStorage.getItem('adobeCredentials');
            if(!credentials){
                return null;
            }

            credentials = JSON.parse(credentials);
            saveCredentials(credentials.username, credentials.password);
            return credentials;
        }

        return {
            setReportSuiteId: setReportSuiteId,
            getSegments: getSegments,
            getMetrics: getMetrics,
            getElements: getElements,
            metricTypes: getAdobeTypes,
            saveCredentialsToLocal: saveCredentialsToLocal,
            getCredentialsFromLocal: getCredentialsFromLocal
        }
    }());

}(jQuery, window.app = window.app || {}, window, document));