(function(app, window, document, undefined) {
    'use strict'

    var adobeReport = app.adobeReport = app.adobeReport || (function() {
       
       var reportType = 'Adobe',
             sharepointFolder = 'Shared Documents\\Adobe';
       

        var report = {
            ReportType: reportType,
            ReportSuiteId: '',
            ExportName: '',
            SharePointFolder: sharepointFolder,
            DateFrom: '',
            DateTo: '',
            DateGranularity: '',
            Metrics: [],
            Segments: [],
            Elements: []
        }
        
        var setMetrics = function(metrics){
            report.Metrics = metrics;
        };

        var setSegments = function(segments){
            report.Segments = segments;
        };

        var setElements = function(elements){
            report.Elements = [];

            for(var i = 0; i < elements.length; i++){
                 var elementObj = {
                    "id": elements[i],
                    "top": 1000
                };
                report.Elements.push(elementObj);
            }
        };

        var setReportSuitId = function(reportSuiteId){
            report.ReportSuiteId = reportSuiteId;
        };

        var setDateFrom = function(dateFrom){
            report.DateFrom = dateFrom;
        };

        var setDateTo = function(dateTo){
            report.DateTo = dateTo;
        };

        var setDateGranularity = function(granularity){
            report.DateGranularity = granularity;
        };

        var setExportName = function(exportName){
            report.ExportName = exportName + '.json';
        };

        var serializeReport = function(){
            return JSON.stringify(report, null, 4);
        }

        return {
            setSegments: setSegments,
            setMetrics: setMetrics,
            setElements: setElements,
            setReportSuiteId: setReportSuitId,
            setDateFrom: setDateFrom,
            setDateTo: setDateTo,
            setDateGranularity: setDateGranularity,
            setExportName: setExportName,
            serializeReport: serializeReport
        }
    }());

}(window.app = window.app || {}, window, document));