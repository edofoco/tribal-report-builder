
(function(jQuery, app, window, document, undefined) {
    'use strict'

    var adobeService = app.adobeService;
    var adobeReport = app.adobeReport;

    var adobeReportBuilder = app.adobeReportBuilder = app.adobeReportBuilder || (function(){
        
        var settings = {
            document: jQuery(document),
            saveCredsButton: jQuery('#btn-save-credentials'),
            usernameInput: jQuery('#username'),
            passwordInput: jQuery('#password'),
            loadButton: jQuery('#btn-load-reportsuite'),
            createReportButton: jQuery('#create-report'),
            searchElementsInput: jQuery('#search-elements'),
            searchMetricsInput: jQuery('#search-metrics'),
            searchSegmentsInput: jQuery('#search-segments'),
            elementsContainer: jQuery('#elements-container'),
            metricsContainer: jQuery('#metrics-container'),
            segmentsContainer: jQuery('#segments-container'),
            reportSuiteInput: jQuery('#report-suite'),
            exportNameInput: jQuery('#export-name'),
            dateFromInput: jQuery('#date-from'),
            dateToInput: jQuery('#date-to'),
            granularityInput: jQuery('#granularity'),
            segmentClass: '.segment',
            metricClass: '.metric',
            elementClass: '.element',
            modal: jQuery('#myModal'),
            modalBody: jQuery('#modal-body')
        };

        var metrics = [],
            segments = [],
            elements = [],
            selectedMetrics = [],
            selectedSegments = [],
            selectedElements = [];

        var drawData = function(container, data, selectedList, type){
            container.empty();

            for (var i = 0, len = data.length; i < len; i++) {
                var checked = '';
                if(selectedList.indexOf(data[i].id) >= 0){
                    checked = 'checked';
                }

                container.append('' +
                    '<div class=row>' +
                    '<label>' +
                    '<input type="checkbox" class="' + type + '" value="' + data[i].id + '" ' + checked +' />  ' + data[i].name +
                    '</label>' +
                    '</div>');
            }
        };

        var loadData = function(data, type){
            switch(type){
                case adobeService.metricTypes().TYPE_ELEMENT:
                    elements = data;
                    drawData(settings.elementsContainer, elements, selectedElements, type);
                    break;
                case adobeService.metricTypes().TYPE_METRIC:
                    metrics = data;
                    drawData(settings.metricsContainer, metrics, selectedMetrics, type);
                    break;
                case adobeService.metricTypes().TYPE_SEGMENT:
                    segments = data;
                    drawData(settings.segmentsContainer, segments, selectedSegments, type);
                    break;
                default:
                    console.log('Invalid type');
            };
        };
        

        var searchData = function(searchTerm, list){
            return jQuery.grep(list, function(e){ return e.name.toLowerCase().indexOf(searchTerm.toLowerCase()) >= 0; });
        };

        var bindUiActions = function(){
         
            settings.saveCredsButton.on('click', function(){
                if(settings.usernameInput.val() == '' || settings.passwordInput == '' ){
                    console.log('Specify both username and password.')
                    return;
                }

                adobeService.saveCredentialsToLocal(settings.usernameInput.val(), settings.passwordInput.val());
            });

            settings.loadButton.on('click', function(){
                if(settings.reportSuiteInput.val() !== ''){
                    adobeService.setReportSuiteId(settings.reportSuiteInput.val());
                    adobeService.getMetrics(loadData);
                    adobeService.getElements(loadData);
                    adobeService.getSegments(loadData);
                }
                else{
                    console.log('Specify a report suite.');
                }
            });

            settings.searchElementsInput.on('keyup', function(){
                var result = searchData(settings.searchElementsInput.val(), elements);
                drawData(settings.elementsContainer, result, selectedElements, adobeService.metricTypes().TYPE_ELEMENT);
            });

             settings.searchMetricsInput.on('keyup', function(){
                var result = searchData(settings.searchMetricsInput.val(), metrics);
                drawData(settings.metricsContainer, result, selectedMetrics, adobeService.metricTypes().TYPE_METRIC);
            });

            settings.searchSegmentsInput.on('keyup', function(){
                var result = searchData(settings.searchSegmentsInput.val(), segments);
                drawData(settings.segmentsContainer, result, selectedSegments, adobeService.metricTypes().TYPE_SEGMENT);
            });
            
            settings.elementsContainer.on('change', settings.elementClass, function(){
                if(jQuery(this).is(':checked')) {
                    if(selectedElements.indexOf(jQuery(this).val()) < 0){
                        selectedElements.push(jQuery(this).val());
                        
                    }
                } else {
                    var elementIndex = selectedElements.indexOf(jQuery(this).val());
                    if( elementIndex >= 0){
                        selectedElements.splice(elementIndex, 1);
                    }
                }
            });

            settings.segmentsContainer.on('change', settings.segmentClass, function(){
                if($(this).is(':checked')) {
                    if(selectedSegments.indexOf(jQuery(this).val()) < 0){
                        selectedSegments.push(jQuery(this).val());
                    }
                } else {
                    var segmentIndex = selectedSegments.indexOf(jQuery(this).val());
                    if( segmentIndex >= 0){
                        selectedSegments.splice(segmentIndex, 1);
                    }
                }
            });

            settings.metricsContainer.on('change', settings.metricClass, function(){
                if(jQuery(this).is(':checked')) {
                    if(selectedMetrics.indexOf(jQuery(this).val()) < 0){
                        selectedMetrics.push(jQuery(this).val());
                    }
                } else {
                    var metricIndex = selectedMetrics.indexOf(jQuery(this).val());
                    if( metricIndex >= 0){
                        selectedMetrics.splice(metricIndex, 1);
                    }
                }
            });

            jQuery(settings.createReportButton).on('click', function(){
                adobeReport.setReportSuiteId(settings.reportSuiteInput.val());
                adobeReport.setElements(selectedElements);
                adobeReport.setMetrics(selectedMetrics);
                adobeReport.setSegments(selectedSegments);
                adobeReport.setDateFrom(settings.dateFromInput.val());
                adobeReport.setDateTo(settings.dateToInput.val());
                adobeReport.setDateGranularity(settings.granularityInput.val());
                adobeReport.setExportName(settings.exportNameInput.val());

                var report = adobeReport.serializeReport();
                 settings.modalBody.html(report);
                 settings.modal.modal();
            });
            
        };


        var loadAdobeCredentials = function(){
           var credentials = adobeService.getCredentialsFromLocal();
           if(credentials){
               settings.usernameInput.attr('placeholder', credentials.username);
               settings.passwordInput.attr('placeholder', credentials.password);
           }
        }

        var init = function(){
            bindUiActions();
            loadAdobeCredentials();
        }

        return {
            init: init
        }
    }());

    adobeReportBuilder.init();

}(jQuery, window.app = window.app || {}, window, document));

