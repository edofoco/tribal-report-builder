var gulp = require('gulp');
var path = require('path');
var less = require('gulp-less');
var minify = require('gulp-minify');
var concat = require('gulp-concat')

gulp.task('copy-html', function(){
    return gulp.src('./src/*.html')
        .pipe(gulp.dest('./dist'));
});

gulp.task('copy-images', function(){
    return gulp.src('./src/assets/img/*')
        .pipe(gulp.dest('./dist/assets/img'))
})

gulp.task('less', function () {
    return gulp.src('./src/assets/less/style.less')
        .pipe(less({
            paths: [ path.join(__dirname, 'less', 'includes') ]
        }))
        .pipe(gulp.dest('./dist/assets/css'));
});


gulp.task('minify-js', function () {
    return gulp.src('./src/assets/js/new/*.js')
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./dist/assets/js'))
        .pipe(minify())
        .pipe(gulp.dest('./dist/assets/js'))
});

gulp.task('default', ['copy-html', 'copy-images', 'less', 'minify-js']);